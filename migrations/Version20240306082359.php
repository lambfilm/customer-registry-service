<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240306082359 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `currencies` (id INT AUTO_INCREMENT NOT NULL, payment_account_id INT DEFAULT NULL, code VARCHAR(10) NOT NULL, INDEX IDX_37C44693AE9DDE6F (payment_account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `customers` (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `payment_accounts` (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, INDEX IDX_8A0EDCC49395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `tokens` (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_AA5A118EA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `users` (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_IDENTIFIER_EMAIL (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `currencies` ADD CONSTRAINT FK_37C44693AE9DDE6F FOREIGN KEY (payment_account_id) REFERENCES `payment_accounts` (id)');
        $this->addSql('ALTER TABLE `payment_accounts` ADD CONSTRAINT FK_8A0EDCC49395C3F3 FOREIGN KEY (customer_id) REFERENCES `customers` (id)');
        $this->addSql('ALTER TABLE `tokens` ADD CONSTRAINT FK_AA5A118EA76ED395 FOREIGN KEY (user_id) REFERENCES `users` (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `currencies` DROP FOREIGN KEY FK_37C44693AE9DDE6F');
        $this->addSql('ALTER TABLE `payment_accounts` DROP FOREIGN KEY FK_8A0EDCC49395C3F3');
        $this->addSql('ALTER TABLE `tokens` DROP FOREIGN KEY FK_AA5A118EA76ED395');
        $this->addSql('DROP TABLE `currencies`');
        $this->addSql('DROP TABLE `customers`');
        $this->addSql('DROP TABLE `payment_accounts`');
        $this->addSql('DROP TABLE `tokens`');
        $this->addSql('DROP TABLE `users`');
    }
}
