## Разворачиваем проект

Для работы проекта на локальной машине должен быть установлен php8.* и зависимости, а так же консоль [symfony-cli](https://symfony.com/download). Сервер будем разворачивать командой php.

#### Первым делом клонируем проект.

```bash
  mkdir "YOUR DIRNAME"
  cd "YOUR DIRNAME"
  git clone git@gitlab.com:lambfilm/customer-registry-service.git .  
  docker-compose up -d
```

Далее экспортируем перемешки, и берем из них драйвер подключения к БД.

```bash
  symfony var:export --multiline
```

Далее вставляем драйвер в .env файл. Перемешка будет выглядеть приблизительно следующим образом:

DATABASE_URL=mysql://root:password@127.0.0.1:32768/main?sslmode=disable&charset=utf8mb4&serverVersion=8.0.36-1.el8

Далее накатываем миграции

[//]: # (symfony console make:migration)
```bash
  symfony console doctrine:migrations:migrate
```

И поднимаем сервер:

```bash
  php -S 127.0.0.1:8000 -t public/
```

## Готово

[API](http://127.0.0.1:8000/api)

[API документация](http://127.0.0.1:8000/api/doc)
