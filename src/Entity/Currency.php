<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\{Get, GetCollection, Post, Delete, Put};
use App\Repository\CurrencyRepository;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    description: 'Include only CRUD..',
    operations: [
        new GetCollection(),
        new Get(),
        new Post,
        new Put(),
        new Delete()
    ]
)]
#[ORM\Entity(repositoryClass: CurrencyRepository::class)]
#[ORM\Table(name: '`currencies`')]
class Currency
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 10)]
    private ?string $code = null;

    #[ORM\ManyToOne(inversedBy: 'currencies')]
    private ?PaymentAccount $paymentAccount = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): static
    {
        $this->code = $code;

        return $this;
    }

    public function getPaymentAccount(): ?PaymentAccount
    {
        return $this->paymentAccount;
    }

    public function setPaymentAccount(?PaymentAccount $paymentAccount): static
    {
        $this->paymentAccount = $paymentAccount;

        return $this;
    }
}
