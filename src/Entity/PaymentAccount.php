<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\{Get, GetCollection, Post, Put, Delete};
use App\Repository\PaymentAccountRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    description: 'Include only CRUD..',
    operations: [
        new GetCollection(),
        new Get(),
        new Post,
        new Put(),
        new Delete()
    ]
)]
#[ORM\Entity(repositoryClass: PaymentAccountRepository::class)]
#[ORM\Table(name: '`payment_accounts`')]
class PaymentAccount
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'paymentAccounts')]
    private ?Customer $customer = null;

    #[ORM\OneToMany(targetEntity: Currency::class, mappedBy: 'paymentAccount')]
    private Collection $currencies;

    public function __construct()
    {
        $this->currencies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection<int, Currency>
     */
    public function getCurrencies(): Collection
    {
        return $this->currencies;
    }

    public function addCurrency(Currency $currency): static
    {
        if (!$this->currencies->contains($currency)) {
            $this->currencies->add($currency);
            $currency->setPaymentAccount($this);
        }

        return $this;
    }

    public function removeCurrency(Currency $currency): static
    {
        if ($this->currencies->removeElement($currency)) {
            if ($currency->getPaymentAccount() === $this) {
                $currency->setPaymentAccount(null);
            }
        }

        return $this;
    }
}
