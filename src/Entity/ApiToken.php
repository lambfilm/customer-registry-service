<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\{Get, Post, Delete, Put, GetCollection};
use App\Repository\ApiTokenRepository;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Attributes as OA;

#[ApiResource(
    description: 'Include only CRUD..',
    operations: [
        new GetCollection(),
        new Get(),
        new Post,
        new Put(),
        new Delete()
    ]
)]
#[ORM\Entity(repositoryClass: ApiTokenRepository::class)]
#[ORM\Table(name: '`tokens`')]
class ApiToken
{
    private const PERSONAL_ACCESS_TOKEN_PREFIX = 'tcp_';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private string $token;

    #[ORM\OneToOne(inversedBy: 'apiToken', cascade: ['persist', 'remove'])]
    private ?User $user = null;

    public function __construct(string $tokenType = self::PERSONAL_ACCESS_TOKEN_PREFIX)
    {
        $this->token = $tokenType . bin2hex(random_bytes(32));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): static
    {
        $this->token = $token;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
