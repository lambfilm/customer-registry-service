<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\{Get, GetCollection, Post, Delete, Put};
use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    description: 'Include only CRUD..',
    operations: [
        new GetCollection(),
        new Get(),
        new Post,
        new Put(),
        new Delete()
    ]
)]
#[ORM\Entity(repositoryClass: CustomerRepository::class)]
#[ORM\Table(name: '`customers`')]
class Customer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToMany(targetEntity: PaymentAccount::class, mappedBy: 'customer')]
    private Collection $paymentAccounts;

    public function __construct()
    {
        $this->paymentAccounts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, PaymentAccount>
     */
    public function getPaymentAccounts(): Collection
    {
        return $this->paymentAccounts;
    }

    public function addPaymentAccount(PaymentAccount $paymentAccount): static
    {
        if (!$this->paymentAccounts->contains($paymentAccount)) {
            $this->paymentAccounts->add($paymentAccount);
            $paymentAccount->setCustomer($this);
        }

        return $this;
    }

    public function removePaymentAccount(PaymentAccount $paymentAccount): static
    {
        if ($this->paymentAccounts->removeElement($paymentAccount)) {
            if ($paymentAccount->getCustomer() === $this) {
                $paymentAccount->setCustomer(null);
            }
        }

        return $this;
    }
}
