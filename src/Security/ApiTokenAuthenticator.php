<?php

namespace App\Security;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\{
    JsonResponse, Request, Response
};
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\{
    AuthenticationException, CustomUserMessageAuthenticationException, UserNotFoundException
};
use Symfony\Component\Security\Http\Authenticator\{
    AbstractAuthenticator,
    Passport\Badge\UserBadge,
    Passport\Passport,
    Passport\SelfValidatingPassport
};

class ApiTokenAuthenticator extends AbstractAuthenticator
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $repository)
    {
        $this->userRepository = $repository;
    }

    public function supports(Request $request): ?bool
    {
        return $this->isTrusted($request);
    }

    public function authenticate(Request $request): Passport
    {
        $token = $this->getCredentials($request);

        if (!$token) {
            throw new CustomUserMessageAuthenticationException('API token is not provided.');
        }

        return new SelfValidatingPassport(
            new UserBadge($token, function($apiToken) {
                $user = $this->userRepository->findByApiToken($apiToken);

                if (!$user) {
                    throw new UserNotFoundException();
                }

                return $user;
            })
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData()),
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function start(Request $request, AuthenticationException $authException = null): JsonResponse
    {
        $data = [
            'message' => 'Authentication Required',
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function isTrusted(Request $request): bool
    {
        return match ($request->getPathInfo()) {
            '/api',
            '/api/doc' => false,
            default => true,
        };
    }

    public function getCredentials(Request $request): ?string
    {
        $bearerHeader = $request->headers->get('Authorization');

        return substr($bearerHeader, 7);
    }
}
